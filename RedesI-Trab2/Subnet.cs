﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedesI_Trab2
{
    class Subnet
    {
        public string subnetAddress;
        public string broadcastAddress;
        public string subnetMask;
        public int prefixSize;
        public string lowerAddress;
        public string higherAddress;
        public double totalAddresses;

        public Subnet(IPv4 ipv4)
        {
            if (ipv4.prefixSize > 0)
            {
                this.prefixSize = ipv4.prefixSize;
                this.subnetMask = calcSubnetMask(this.prefixSize);              

            } else if (ipv4.subnetMask != null) {
                this.subnetMask = ipv4.subnetMask;
                this.prefixSize = calcPrefixSize(this.subnetMask);
            }

            this.totalAddresses = calcTotalAddresses(this.prefixSize);
            this.subnetAddress = calcSubnetAddress(ipv4.address, this.prefixSize);
            this.broadcastAddress = calcBroadcastAddress(ipv4.address, this.prefixSize);
            this.lowerAddress = calcLowerAddress(this.subnetAddress);
            this.higherAddress = calcHigherAddress(this.broadcastAddress);
        }

        /// <summary>
        /// Calcula tamanho do prefixo com base na máscara de sub-rede
        /// </summary>
        /// <param name="subnetMask"></param>
        /// <returns></returns>
        private int calcPrefixSize(string subnetMask)
        {
            string[] splitMask = subnetMask.Split('.');
            string binaryMask = "";

            foreach(string mask in splitMask)
            {
                binaryMask += Utils.Int32ToBinary(int.Parse(mask), 8);
            }

            int idx = 0;

            foreach(char c in binaryMask)
            {
                if (c == '0')
                    break;

                idx++;
            }

            return idx;
        }

        /// <summary>
        /// Calcula o número total de endereços disponíveis com base no tamanho do prefixo
        /// </summary>
        /// <param name="prefixSize"></param>
        /// <returns></returns>
        private double calcTotalAddresses(int prefixSize)
        {
            double bitsForHosts = 32 - prefixSize;
            double totalAddresses = Math.Pow(2, bitsForHosts) - 2;

            return totalAddresses;
        }

        /// <summary>
        /// Calcula máscara de sub-rede com base no tamanho do prefixo
        /// </summary>
        /// <param name="prefixSize"></param>
        /// <returns></returns>
        private string calcSubnetMask(int prefixSize)
        {
            string binarySubnet = "";
            for (int i = 0; i < 32; i++)
            {
                binarySubnet = i < prefixSize ? binarySubnet + "1" : binarySubnet + "0";
            }

            string result = "";

            for (int i = 0; i < 4; i++)
            {
                if (i > 0)
                {
                    result += ".";
                }
                result += Utils.BinaryToInt32(binarySubnet.Substring(i * 8, 8));
            }
            
            return result;
        }

        /// <summary>
        /// Calcula endereço da sub-rede com base no endereço IPv4 e tamanho do prefixo
        /// </summary>
        /// <param name="ipv4Address"></param>
        /// <param name="prefixSize"></param>
        /// <returns></returns>
        private string calcSubnetAddress(string ipv4Address, int prefixSize)
        {
            string[] splitAddress = ipv4Address.Split('.');

            string binaryAddress = "";            

            for (int i = 0; i < 4; i++)
            {
                binaryAddress += Utils.Int32ToBinary(int.Parse(splitAddress[i]), 8);
            }

            string _binaryAddress = "";

            for (int i = 0; i < 32; i++)
            {
                if (i < prefixSize)
                {
                    _binaryAddress += binaryAddress[i];
                } else {
                    _binaryAddress += "0";
                }
            }

            string result = "";

            for (int i = 0; i < 4; i++)
            {
                if (i > 0)
                {
                    result += ".";
                }
                result += Utils.BinaryToInt32(_binaryAddress.Substring(i * 8, 8));
            }

            result += "/" + prefixSize;

            return result;
        }

        /// <summary>
        /// Calcula endereço de broadcast com base no endereço IPv4 e tamanho do prefixo
        /// </summary>
        /// <param name="ipv4Address"></param>
        /// <param name="prefixSize"></param>
        /// <returns></returns>
        private string calcBroadcastAddress(string ipv4Address, int prefixSize)
        {
            string[] splitAddress = ipv4Address.Split('.');

            string binaryAddress = "";

            for (int i = 0; i < 4; i++)
            {
                binaryAddress += Utils.Int32ToBinary(int.Parse(splitAddress[i]), 8);
            }

            string _binaryAddress = "";

            for (int i = 0; i < 32; i++)
            {
                if (i < prefixSize)
                {
                    _binaryAddress += binaryAddress[i];
                }
                else
                {
                    _binaryAddress += "1";
                }
            }

            string result = "";

            for (int i = 0; i < 4; i++)
            {
                if (i > 0)
                {
                    result += ".";
                }
                result += Utils.BinaryToInt32(_binaryAddress.Substring(i * 8, 8));
            }

            return result;
        }

        /// <summary>
        /// Calcula primeiro endereço disponível com base no endereço da sub-rede
        /// </summary>
        /// <param name="subnetAddress"></param>
        /// <returns></returns>
        private string calcLowerAddress(string subnetAddress)
        {
            string[] splitAddress = subnetAddress.Split('/');
            splitAddress = splitAddress[0].Split('.');

            splitAddress[3] = (int.Parse(splitAddress[3]) + 1).ToString();
            if (splitAddress[3] == "256")
            {
                splitAddress[3] = "0";
                splitAddress[2] = (int.Parse(splitAddress[2]) + 1).ToString();

                if (splitAddress[2] == "256")
                {
                    splitAddress[2] = "0";
                    splitAddress[1] = (int.Parse(splitAddress[1]) + 1).ToString();

                    if (splitAddress[1] == "256")
                    {
                        splitAddress[1] = "0";
                        splitAddress[0] = (int.Parse(splitAddress[0]) + 1).ToString();

                        if (splitAddress[0] == "256")
                        {
                            throw new Exception("Something went pretty wrong...");
                        }
                    }
                }
            }

            string result = "";

            for (int i = 0; i < 4; i++)
            {
                if (i > 0)
                {
                    result += ".";
                }
                result += splitAddress[i];
            }

            return result;
        }

        /// <summary>
        /// Calcula útlimo endereço disponível com base no endereço de broadcast
        /// </summary>
        /// <param name="broadcastAddress"></param>
        /// <returns></returns>
        private string calcHigherAddress(string broadcastAddress)
        {
            string[] splitAddress = broadcastAddress.Split('.');

            splitAddress[3] = (int.Parse(splitAddress[3]) - 1).ToString();
            if (splitAddress[3] == "-1")
            {
                splitAddress[3] = "255";
                splitAddress[2] = (int.Parse(splitAddress[2]) - 1).ToString();

                if (splitAddress[2] == "-1")
                {
                    splitAddress[2] = "255";
                    splitAddress[1] = (int.Parse(splitAddress[1]) - 1).ToString();

                    if (splitAddress[1] == "-1")
                    {
                        splitAddress[1] = "255";
                        splitAddress[0] = (int.Parse(splitAddress[0]) - 1).ToString();

                        if (splitAddress[0] == "-1")
                        {
                            throw new Exception("Something went pretty wrong...");
                        }
                    }
                }
            }

            string result = "";

            for (int i = 0; i < 4; i++)
            {
                if (i > 0)
                {
                    result += ".";
                }
                result += splitAddress[i];
            }

            return result;
        }                
    }
}
