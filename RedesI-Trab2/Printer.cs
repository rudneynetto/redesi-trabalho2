﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedesI_Trab2
{
    /// <summary>
    /// Classe responsável por imprimir informações em tela
    /// </summary>
    class Printer
    {
        public static void Clear()
        {
            Console.Clear();
        }

        public static void PrintCabecalho()
        {
            Clear();
            Console.WriteLine("#############################################################");
            Console.WriteLine("#  Redes I - Ferramenta para segmentação de sub-redes IPv4  #");
            Console.WriteLine("#  Aluno: Rudney Netto                Matrícula: 114083038  #");
            Console.WriteLine("#############################################################\n\n");
        }

        public static void PrintMenu()
        {
            Console.WriteLine("-- MODOS DISPONÍVEIS\n");
            Console.WriteLine("1 - Modo de Informações: o programa receberá como argumento uma sub-rede IPv4 e imprimirá na tela informações como o endereço de broadcast, e o número máximo de hosts endereçáveis.\n");
            Console.WriteLine("2 - (DESATIVADO) Modo de divisão em sub-redes menores com prefixo de tamanho fixo: o programa receberá como argumento uma sub-rede IPv4 e um novo tamanho de prefixo e imprimirá informações acerca de todas as sub-redes com o tamanho de prefixo informado que se encontram dentro da sub-rede original.\n");
            Console.WriteLine("3 - (DESATIVADO) Modo de divisão em sub-redes menores com prefixos variáveis: o programa receberá como argumento uma sub-rede IPv4 e um ou mais valores inteiros informando as quantidades de hosts que devem ser endereçados nas sub-redes menores a serem criadas a partir da sub-rede original. O programa, então, calculará a divisão mais eficiente da sub-rede original nas novas sub-redes e, para cada nova sub-rede gerada, imprimirá uma série de informações.\n");
            Console.Write("Digite o modo desejado: ");
        }

        public static void PrintInformacoesInput()
        {
            Console.WriteLine("MODO SELECIONADO: 1\n");
            Console.WriteLine("Entre com uma sub-rede no formato \"{Endereço}/{Tam. do Prefixo}\" ou \"{Endereço} {Máscara de Sub-rede}\"");
            Console.Write("Sub-rede: ");
        }

        public static void PrintSubnetInfo(Subnet subnet)
        {
            Console.WriteLine("\n\n- Informações da sub-rede\n");
            Console.WriteLine("Endereço de Sub-Rede: " + subnet.subnetAddress);
            Console.WriteLine("Endereço de Sub-Rede (Binário): " + Utils.IpToBinaryIp(subnet.subnetAddress, true) + "\n");
            Console.WriteLine("Endereço de Broadcast: " + subnet.broadcastAddress);
            Console.WriteLine("Endereço de Broadcast (Binário): " + Utils.IpToBinaryIp(subnet.broadcastAddress) + "\n");
            Console.WriteLine("Máscara de Sub-Rede: " + subnet.subnetMask);
            Console.WriteLine("Máscara de Sub-Rede (Binário): " + Utils.IpToBinaryIp(subnet.subnetMask) + "\n");
            Console.WriteLine("Tamanho do Prefixo: " + subnet.prefixSize + "\n");
            Console.WriteLine("Primeiro Endereço: " + subnet.lowerAddress);
            Console.WriteLine("Primeiro Endereço (Binário): " + Utils.IpToBinaryIp(subnet.lowerAddress) + "\n");
            Console.WriteLine("Último Endereço: " + subnet.higherAddress);
            Console.WriteLine("Último Endereço (Binário): " + Utils.IpToBinaryIp(subnet.higherAddress) + "\n");
            Console.WriteLine("Total de Endereços: " + subnet.totalAddresses);
        }
    }
}
