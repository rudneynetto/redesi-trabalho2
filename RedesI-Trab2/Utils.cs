﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedesI_Trab2
{
    class Utils
    {
        /// <summary>
        /// Converte string de binário em um inteiro
        /// </summary>
        /// <param name="binary"></param>
        /// <returns></returns>
        public static int BinaryToInt32(string binary)
        {
            int value = Convert.ToInt32(binary, 2);
            return value;
        }

        /// <summary>
        /// Converte inteiro em string de binário
        /// </summary>
        /// <param name="number"></param>
        /// <param name="characterCount"></param>
        /// <returns></returns>
        public static string Int32ToBinary(int number, int characterCount = 0)
        {
            string value = Convert.ToString(number, 2);

            while (value.Length < characterCount)
            {
                value = "0" + value;
            }

            return value;
        }

        /// <summary>
        /// Converte IP em base 10 para IP em base binária
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="isSubnetAddress"></param>
        /// <returns></returns>
        public static string IpToBinaryIp(string ip, bool isSubnetAddress = false)
        {
            string result = "";

            string[] splitAddress;

            if (isSubnetAddress)
            {
                splitAddress = ip.Split('/');
                splitAddress = splitAddress[0].Split('.');
            }
            else
            {
                splitAddress = ip.Split('.');
            }

            for (int i = 0; i < 4; i++)
            {
                if (i > 0)
                {
                    result += ".";
                }

                result += Utils.Int32ToBinary(int.Parse(splitAddress[i]), 8);
            }

            return result;
        }
    }
}
