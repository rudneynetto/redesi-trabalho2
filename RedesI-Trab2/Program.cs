﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedesI_Trab2
{
    public enum ModesEnum
    {
        Informacoes = 1,
        DivisaoFixo = 2,
        DivisaoVariavel = 3
    }

    class Program
    {
        /// <summary>
        /// Máquina de estados da aplicação
        /// 0: Startup
        /// 1: Menu Principal
        /// </summary>
        static int state;

        /// <summary>
        /// Modo do programa atualmente selecionado
        /// 1: Modo de Informações
        /// 2: Modo de divisão de sub-redes menores com prefixo de tamanho fixo
        /// 3: Modo de divisão de sub-redes menores com prefixos variáveis
        /// </summary>
        static int mode;
        
        static Validator validator = new Validator();

        static void Main(string[] args)
        {
            state = 0;

            ResolveState();

            Console.ReadLine();
        }

        static void ResolveState()
        {
            switch(state)
            {
                case 0:
                    ExecMenu();
                    break;
                case 1:
                    ResolveMode();
                    break;
                default:
                    break;
            }
        }

        static void ResolveMode()
        {
            switch(mode)
            {
                case 1:
                    ExecModoInformacoes();
                    break;
                default:
                    Console.WriteLine("ERROR: Modo inválido ou desativado");
                    break;
            }
        }

        static void ExecMenu()
        {
            try
            {
                state = 1;
                
                Printer.PrintCabecalho();
                Printer.PrintMenu();
                mode = int.Parse(Console.ReadLine());

                if (Enum.IsDefined(typeof(ModesEnum), mode)) {
                    ResolveState();
                } else {
                    Console.WriteLine("ERROR: Modo inválido");
                }

            } catch (Exception e) {
                Console.WriteLine("ERROR: " + e.Message);
            }
        }

        static void ExecModoInformacoes()
        {
            try
            {
                state = 2;
                string input;
                
                Printer.PrintCabecalho();
                Printer.PrintInformacoesInput();
                input = Console.ReadLine();

                IPv4 ipv4 = new IPv4(input);
                Subnet subnet = new Subnet(ipv4);
                Printer.PrintSubnetInfo(subnet);
            } catch (Exception e) {
                Console.WriteLine("ERROR: " + e.Message);
            }       
        }
    }
}
