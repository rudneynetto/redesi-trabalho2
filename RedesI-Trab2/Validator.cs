﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedesI_Trab2
{
    /// <summary>
    /// Classe responsável pela validação de dados
    /// Ex: Validação de IPv4
    /// </summary>
    class Validator
    {
        /// <summary>
        /// Valida valores dos octetos do endereço de rede
        /// Valida valores dos octetos da máscara de sub-rede
        /// Valida tamanho do prefixo
        /// </summary>
        /// <param name="ipv4"></param>
        public static void validateIPv4(IPv4 ipv4)
        {
            string[] splitAddress = ipv4.address.Split('.');

            foreach (string address in splitAddress)
            {
                if (int.Parse(address) > 255 || int.Parse(address) < 0)
                {
                    throw new Exception("Endereço de rede inválido");
                }
            }

            if (ipv4.subnetMask != null)
            {
                string[] splitMask = ipv4.subnetMask.Split('.');

                foreach(string mask in splitMask)
                {
                    if (int.Parse(mask) > 255 || int.Parse(mask) < 0)
                    {
                        throw new Exception("Máscara de sub-rede inválida");
                    }
                }
            }

            if ((ipv4.prefixSize < 0 || ipv4.prefixSize > 32) && (ipv4.prefixSize != -1))
            {
                throw new Exception("Tamanho do prefixo inválido");
            }
        }
    }
}
