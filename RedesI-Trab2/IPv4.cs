﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedesI_Trab2
{
    class IPv4
    {
        public string address = null;
        public int prefixSize = -1;
        public string subnetMask = null;

        public IPv4(string fullAddress)
        {
            string[] fullAddressPrefix;
            string[] fullAddressMask;

            fullAddressPrefix = fullAddress.Split('/');
            fullAddressMask = fullAddress.Split(' ');

            // Checa se o IPv4 foi criado através de máscara de sub-rede ou tamanho de prefixo
            if (fullAddressPrefix.Length > 1)
            {
                address = fullAddressPrefix[0];
                prefixSize = int.Parse(fullAddressPrefix[1]);
            } else if (fullAddressMask.Length > 1)
            {
                address = fullAddressMask[0];
                subnetMask = fullAddressMask[1];
            } else
            {
                throw new Exception("Entrada de IPv4 em formato inválido");
            }

            Validator.validateIPv4(this);
        }
    }
}
